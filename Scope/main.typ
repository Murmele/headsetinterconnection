//1.png: Adapter v1.0 Voltage MicrophoneAdapter < - > GND
//2.png: Adapter v1.1 Voltage MicrophoneAdapter < - > GND
//	- Much smaller. Only ~0.7V

// AdapterV1.1_UGS_NoButtonPressed.png: Adapter v1.1 Gate Voltage. No Microphone Button Press
// AdapterV1.0_UGS_NoButtonPressed.png: Adapter v1.0 Gate Voltage. No Microphone Button Press
#figure(
	grid(
	columns: (1fr, 1fr, 1fr),
	gutter: 10pt,
	figure(
		image("images/AdapterV1.0_UGS_NoButtonPressed.png", width: 100%),
		caption: [Adapter v1.0]
	),
	figure(
		image("images/AdapterV1.1_UGS_NoButtonPressed.png", width: 100%),
		caption: [Adapter v1.1]
	),
	),
	caption: [U_GS Button not pressed]
)
When Pressing the microphone Button both gates get pulled low

//GND < - > Microphone Radio Button not pressed:
//	- Adapter v1.0: AdapterV1.0_GND_Microphone_ButtonNotPressed.png
//	- Adapter v1.1: AdapterV1.1_GND_Microphone_ButtonNotPressed.png
//	- Adapter v1.1 Floating aufbau (Kabel):AdapterV1.1_Cable_GND_Microphone_ButtonNotPressed.png
#figure(
	grid(
		columns: (1fr, 1fr, 1fr),
		gutter: 10pt,
		figure(
			image("images/AdapterV1.0_GND_Microphone_ButtonNotPressed.png", width: 100%),
			caption: [Adapter v1.0]
		),
		figure(
			image("images/AdapterV1.1_GND_Microphone_ButtonNotPressed.png", width: 100%),
			caption: [Adapter v1.1]
		),
		figure(
			image("images/AdapterV1.1_Cable_GND_Microphone_ButtonNotPressed.png", width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel)]
		)
	),
	caption: [GND < - > Microphone Radio. Button not pressed:]
)

// GND < - > Microphone Radio
// 	- Button Pressed:
// 		- Adapter v1.0: 1.5V AdapterV1.0_GND_Microphone_ButtonPressed.png
// 		- Adapter v1.1: 1.8V AdapterV1.1_GND_Microphone_ButtonPressed.png
// 		- Adapter v1.1 Floating aufbau (Kabel): 2.35V: AdapterV1.1_Cable_GND_Microphone_ButtonPressed.png
#figure(
	grid(
		columns: (1fr, 1fr, 1fr),
		gutter: 0pt,
		figure(
			image("images/AdapterV1.0_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.0 (1.5V)]
		),
		figure(
			image("images/AdapterV1.1_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.1 (1.8V)]
		),
		figure(
			image("images/AdapterV1.1_Cable_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel) (2.35V) (*Broken transistor*)]
		),
		figure(
			image("images/AdapterV1.1_Cable_NoR2C2_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel) C1 and R2 not connected (*Broken transistor*)]
		),
		figure(
			image("images/AdapterV1.1_Cable_R2C2_DirectlyOnRadioCinch_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel) C1 and R2 are connected directly to the Cinch]
		),

		figure(
			image("images/AdapterV1.1_Cable_NoR2C2_WorkingTransistor_GND_Microphone_ButtonPressed.png", width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel) No C1 and no R2]
		),

		figure(
			image("images/AdapterV1.1_Cable_R2OnlyNoC1_GND_Microphone_ButtonPressed.png",
			width: 100%),
			caption: [Adapter v1.1 Floating aufbau (Kabel) No C1, Only R2]
		),
	),
	caption: [GND < - > Microphone Radio. Button pressed. No Headset connected. Only the Remote Button cable]
)

If R2 and C1 are directly soldered to the pcb it looks same as images/AdapterV1.1_Cable_GND_Microphone_ButtonPressed.png. 
- Also when the Ground cable (black) was removed and the 2.5mm cinch was soldered with the two relevant pins on the pcb the signal looked still the same.
- Same when removing all loose cables
- Same when all 3.5mm cinch connectors are soldered completely
- *Changed transistor helped (Broken transistor)!*

Changing R2 did not change anything on the signal. Same level, same noise

#figure(
	grid(
		columns: (1fr, 1fr),
		gutter: 10pt,
		figure(
			image("images/AdapterV1.1_Cable_LooseGND.png", width: 100%),
			caption: [Adapter V1.1 With Cables and Loose GND]
		),
		figure(
			image("images/AdapterV1.1_Cable_Bottom.png", width: 100%),
			caption: [Adapter V1.1 (Floating Aufbau) with all cables removed and R2 and C1 directly on pcb. Not all pins are soldered for easily modification]
		),
	),
)

#figure(
	grid(
		columns: (1fr, 1fr, 1fr),
		gutter: 0pt,
		figure(
			image("images/AdapterV1.0_GND_Microphone_ButtonPressed_MicrophoneConnected.png", width: 100%),
			caption: [Adapter v1.0]
		),
	
		

		
	),
	caption: [GND < - > Microphone Radio. Button pressed. *Headset connected*. Only the Remote Button cable]
)

#figure(
	grid(
		columns: (1fr, 1fr),
		gutter: 0pt,
		// No button pressed
		figure(
			image("images/AdapterV1.0_GND_Microphone_NoButtonPressed_MicrophoneConnected_AC.png", width: 100%),
			caption: [Adapter v1.0 No Button Pressed]
		),

		figure(
			image("images/AdapterV1.1_Cable_R2Only_GND_Microphone_NoButtonPressed_MicrophoneConnected_AC.png", width: 100%),
			caption: [Adapter v1.1 Floating No Button Pressed R2 Only]
		),

		// Button pressed
		figure(
			image("images/AdapterV1.0_GND_Microphone_ButtonPressed_MicrophoneConnected_AC.png", width: 100%),
			caption: [Adapter v1.0 Button Pressed]
		),
		figure(
			image("images/AdapterV1.1_Cable_R2Only_GND_Microphone_ButtonPressed_MicrophoneConnected_AC.png", width: 100%),
			caption: [Adapter v1.1 Floating Button Pressed R2 Only]
		),

		// Ins Micro Blasen
		figure(
			image("images/AdapterV1.0_GND_Microphone_ButtonPressed_MicrophoneConnected_AC_InsMicroblasen.png", width: 100%),
			caption: [Adapter v1.0 Button Pressed Ins Microphon Blasen]
		),

		figure(
			image("images/AdapterV1.1_Cable_R2Only_GND_Microphone_ButtonPressed_MicrophoneConnected_AC_InsMicroblasen.png", width: 100%),
			caption: [Adapter v1.1 Floating Button Pressed R2 Only Ins Microphon Blasen. Only a little bit visible]
		),
	),
	caption: [GND < - > Microphone Radio. *Headset connected* AC Signal]
)



#figure(
	grid(
		columns: (1fr, 1fr),
		gutter: 0pt,

		figure(
			image("images/AdapterV1.0_GND_MicrophoneAdapter_ButtonPressed_MicrophoneConnected.png", width: 100%),
			caption: [Adapter V1.0]
		),


		figure(
			image("images/AdapterV1.1_Cable_R2Only_GND_MicrophoneAdapter_ButtonPressed_MicrophoneConnected.png", width: 100%),
			caption: [Adapter V1.1 Floating]
		),
	),
	caption: [GND < - > Microphone Adapter (Voltage accross C1). *Headset connected*]
)
For some reason, the drop accross the Transistor is different in V1.0 and V1.1. Don't remeber which transistor is on the V1.0. On V1.1 is an *Infineon BSS84PH6327XTSA2*

DS Voltage HeadsetAdapter V1.0: 0V with Signal when pressed, otherwise around 3V
DS Voltage HeadsetAdapter V1.1: 0.7V with Signal when pressed, otherwise around 3V









