Headsetinterconnection PCB

PCB To connect a headset with 3.5mm cinch to extender cable with remote press button to a 2.5mm cich connector.
If you have already the double cich connector, you don't need the second pcb it is just for double cinch to 2.5mm cinch

![Image](https://gitlab.com/Murmele/headsetinterconnection/-/raw/master/HeadsetInterconnection.jpg)

![Schematic](https://gitlab.com/Murmele/headsetinterconnection/-/raw/master/Schematic.png)

Bill of material

| Reference      | PCB Part | Quantity | Description         | Manufacturer   | Partnumber   | Supplier                        | Supplier Number | Url                                                                                                                                                      |
|----------------|----------|----------|---------------------|----------------|--------------|---------------------------------|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| J1, J3, J5     |    yes   |     3    | AudioJack4          |                |              | Conrad                          | 718732 - AW     | https://www.conrad.de/de/p/conrad-components-718732-klinken-steckverbinder-3-5-mm-buchse-einbau-horizontal-polzahl-4-stereo-schwarz-1-st-718732.html     |
| J2, J4, J6, J7 |    yes   |     4    | AudioJack4_SwitchT  |                |              | Conrad                          | 1578843 - AW    | https://www.conrad.de/de/p/tru-components-1578843-klinken-steckverbinder-2-5-mm-buchse-einbau-horizontal-polzahl-4-1-st-1578843.html                     |
| N/A            |    no    |     1    | Remote press button |                |              | Amazon                          |                 | https://www.amazon.de/gp/product/B07PLYD2FN/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1                                                             |
| N/A            |    no    |     1    | Headset             |                |              | Amazon                          |                 |                                                                                                                                                          |


